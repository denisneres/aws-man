package com.example.demo;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyRequest;
import com.amazonaws.services.identitymanagement.model.CreateAccessKeyResult;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAJPXDTRFSLPPWIU6A", "n7l5ZHiuJ0BeYWG3tAYfXTmlgDHpnn6AoDmUKu/A");
		final AmazonIdentityManagement iam =
				AmazonIdentityManagementClientBuilder
						.standard()
						.withRegion(String.valueOf(Region.getRegion(Regions.US_EAST_2))).withCredentials(new AWSStaticCredentialsProvider(awsCreds))
						.build();

		CreateAccessKeyRequest request = new CreateAccessKeyRequest().withUserName("denis-dev");

		CreateAccessKeyResult result = iam.createAccessKey(request);

		System.out.println("Created access key");
	}
}
